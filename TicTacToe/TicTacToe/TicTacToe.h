//Tic Tac Toe header file

#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>



class TicTacToe
{

private:

	int m_moveCounter = 0;
	char m_winner = ' ';
	char m_board[10] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	int m_numTurns;
	char m_playerTurn = 'X';

public:




	void DisplayBoard()
	{

		//std::cout << "\n\n\tTic Tac Toe\n\n";

		std::cout << "Player 1 (X) - Player 2 (O)" << "\n" << "\n";
		std::cout << "\n";

		std::cout << "\t" << "____________" << "\n";
		std::cout << "\t" << "|   |   |   |" << "\n";
		std::cout << "\t" << "|" << " " << m_board[1] << " | " << m_board[2] << " | " << m_board[3] << " | " << "\n";

		std::cout << "\t" << "|___|___|___|" << "\n";
		std::cout << "\t" << "|   |   |   |" << "\n";

		std::cout << "\t" << "|" << " " << m_board[4] << " | " << m_board[5] << " | " << m_board[6] << " | " << "\n";

		std::cout << "\t" << "|___|___|___|" << "\n";
		std::cout << "\t" << "|   |   |   |" << "\n";

		std::cout << "\t" << "|" << " " << m_board[7] << " | " << m_board[8] << " | " << m_board[9] << " | " << "\n";

		std::cout << "\t" << "|___|___|___|" << "\n" << "\n";
	}

	bool IsOver()
	{

		return 0;
	}

	char GetPlayerTurn()
	{

		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{



		if (m_board[position] != 'X' && m_board[position] != 'O')
		{
			return true;
		}

		return false;
	}



	void Move(int position)
	{

		m_board[position] = GetPlayerTurn();


		m_moveCounter++;

		//Ignores iteration if counter is less than 5 since win/tie impossible
		if (m_moveCounter >= 5)
		{

			//Test rows
			for (int rowCounter = 1; rowCounter < 8; rowCounter += 3)
			{

				if (m_board[rowCounter] == m_board[rowCounter + 1] && m_board[rowCounter + 1] == m_board[rowCounter + 2])
				{

					m_winner = GetPlayerTurn();
					DisplayResult();

				}
			}

			//Tests columns
			for (int columnCounter = 0; columnCounter < 3; columnCounter++)
			{

				if (m_board[columnCounter] == m_board[columnCounter + 3] && m_board[columnCounter + 3] == m_board[columnCounter + 6])
				{
					m_winner = GetPlayerTurn();
					DisplayResult();
				}
			}

			//Tests both diagnols
			if (m_board[0] == m_board[4] && m_board[4] == m_board[8])
			{

				m_winner = GetPlayerTurn();
				DisplayResult();

			}
			else if (m_board[2] == m_board[4] && m_board[4] == m_board[6])
			{
				m_winner = GetPlayerTurn();
				DisplayResult();
			}

			//Checks for Tie
			if (m_moveCounter == 9 && m_winner != GetPlayerTurn())
			{
				m_winner = GetPlayerTurn();
				DisplayResult();
			}
		
		}


		m_playerTurn = (m_playerTurn == 'X') ? 'O' : 'X';
		

	}

	void DisplayResult()
	{

		std::cout << "\n \n" << "Winner is " << m_winner << "\n \n \n";

	}

};


